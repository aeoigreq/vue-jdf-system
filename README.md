# vue-jdf-system

#### 介绍
vue-jdf-system是分布式文件管理服务的前端模块，采用了前后端分离的设计模式。基于vue-next-admin前端框架二次开发，详情请移步：https://gitee.com/lyt-top/vue-next-admin

#### 软件架构
软件架构说明


#### 安装教程

1.  clone 工程；
2.  执行指令 yarn run dev

#### 使用说明

1.  登录功能暂时使用第三方服务，可使用nginx代理到https:/www.juque.top，演示官网目前也是使用这种方式；
2.  工程使用技术栈：vue3、elementUI、vite，对浏览器版本要求较高；
3.  基于vue-next-admin前端框架二次开发，详情请移步：https://gitee.com/lyt-top/vue-next-admin；
4.  感谢vue-next-admin作者的技术开源，如果大家也觉得vue-next-admin不错，也给一个star吧。开源不易，从你我开始。
5. 演示网站：https://www.juque.top/jdf

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
