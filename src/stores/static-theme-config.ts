import { defineStore } from 'pinia';
import { StaticThemeConfigModel, StaticThemeConfigsModel } from '/@/interfaces/JdfsystemModel';

export const useStaticThemeConfig = defineStore('staticThemeConfig', {
	state: (): StaticThemeConfigsModel => ({
		config: {
			/**
			 * 全局网站标题 / 副标题
			 */
			// 网站主标题（菜单导航、浏览器当前网页标题）
			globalTitle: 'jdf-system',
			// 网站副标题（登录页顶部文字）
			globalViceTitle: 'jdf-system',
			// 是否开启 Footer 底部版权信息
			isFooter: true,
		}
	}),
	actions: {
		getConfig(): StaticThemeConfigModel {
			return this.config;
		}
	}
});