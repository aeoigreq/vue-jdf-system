import { defineStore } from 'pinia';
import { RouteRecordRaw } from 'vue-router';
import { TagViewInfoModel } from '/@/interfaces/RouterModel';
import { Session } from '/@/utils/storage';
import CONSTANTS from '/@/enums/global-constant-enum';

export const useTagViewInfoStore = defineStore('tag-view-info', {
    state: (): TagViewInfoModel => ({
        tagViewList: [],
        isTagsViewCurrenFull: false,
        cacheChange: false,
    }),
    actions: {
        setTagViewList: function (list: RouteRecordRaw[]) {
            let result: RouteRecordRaw[] = [];
            list.forEach(f=>result.push({...f}));
            this.tagViewList = [...result];
            Session.set(CONSTANTS.SESSION_TAG_VIEW_LIST, JSON.stringify(result));
            this.cacheChange = true;
        },
        getTagViewList: function (): RouteRecordRaw[] {
            if(this.tagViewList.length > 0) {
                return this.tagViewList;
            }
            const cache: [] = JSON.parse(Session.get(CONSTANTS.SESSION_TAG_VIEW_LIST) || '[]');
            if(cache.length > 0) {
                this.tagViewList = [...cache];
                return cache;
            }
            return [];
        },
        setIsTagsViewCurrenFull(b: boolean | false) {
            Session.set(CONSTANTS.SESSION_IS_Tag_View_Curren_Full, b+'');
            this.isTagsViewCurrenFull = b;
        },
        getIsTagsViewCurrenFull() {
            if(this.isTagsViewCurrenFull) {
                return true;
            }
            const b = Session.get(CONSTANTS.SESSION_IS_Tag_View_Curren_Full);
            if(b) {
                this.isTagsViewCurrenFull = true;
                return true;
            }
            return false;
        },
        restCacheChange() {
            this.cacheChange = false;
        },
        getCacheChange() {
            return this.cacheChange;
        },
    }
});