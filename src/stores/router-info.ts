import { RouteRecordRaw } from 'vue-router';
import { defineStore } from 'pinia';
import { RouterInfoModel } from '/@/interfaces/RouterModel';
import { useKeepALiveName } from './keep-alive-name';
import pinia from '.';
import { notFoundAndNoPower, staticRoutes } from '/@/router/routers/static-router';
import { dynamicRoutes } from '/@/router/routers/dynamic-router';
import useObjectUtil from '../utils/objectUtil';

export const useRouterInfoStore = defineStore('router-info', {
    state: (): RouterInfoModel => ({
        routeList: [],
        simpleDynamicRouteList: [],
        dynamicRouteNameSet: new Set<string>(),
        hasLoad: false,
        columnsNavHover: false,
        columnsMenuHover: false,
    }),
    actions: {
        getRouteList: function (): RouteRecordRaw[] {
            return [...staticRoutes, ...dynamicRoutes, ...notFoundAndNoPower];
        },
        getSimpleRouteList: function (): RouteRecordRaw[] {
            return this.routeList;
        },
        getSimpleDynamicRouteList: function (): RouteRecordRaw[] {
            return this.simpleDynamicRouteList;
        },
        getDynamicRoutes(): RouteRecordRaw[] {
            const routes: RouteRecordRaw[] = [...dynamicRoutes];
            // 排除根目录
            const children = routes[0].children;
            if (undefined === children) {
                return [];
            }
            return [...children];
        },
        getHasLoad() {
            return this.hasLoad;
        },
        getColumnsNavHover() {
            return this.columnsNavHover;
        },
        getColumnsMenuHover() {
            return this.columnsMenuHover;
        },
        setColumnsNavHover(b: boolean) {
            this.columnsNavHover = b;
        },
        setColumnsMenuHover(b: boolean) {
            this.columnsMenuHover = b;
        },
        isDynamicRoute(name: string | null | undefined) {
            if (null === name || undefined === name) {
                return false;
            }
            return this.dynamicRouteNameSet.has(name);
        },
        buildRouter: function () {
            if (this.hasLoad) {
                return;
            }
            console.info('start--------------平铺路由信息');
            let routeResult: RouteRecordRaw[] = [];
            staticRoutes.forEach(f => {
                // 加载全局路由平铺的一维数组
                const children = this.getChildren(f);
                routeResult = [...routeResult, ...children, ...[f]];
            })
            dynamicRoutes.forEach(f => {
                // 加载全局路由平铺的一维数组
                const children = this.getChildren(f);
                routeResult = [...routeResult, ...children, ...[f]];
                // 加载Set数据结构
                if (useObjectUtil.isNotNull(f.name)) {
                    this.dynamicRouteNameSet.add(f.name as string);
                }
                children.filter(o => useObjectUtil.isNotNull(o.name)).forEach(o => this.dynamicRouteNameSet.add(o.name as string));
                // 加载动态路由平铺的一维数组
                this.simpleDynamicRouteList = [...this.simpleDynamicRouteList, ...children, ...[f]];
            })
            notFoundAndNoPower.forEach(f => {
                // 加载全局路由平铺的一维数组
                const children = this.getChildren(f);
                routeResult = [...routeResult, ...children, ...[f]];
            });
            this.routeList = [...routeResult];
            console.info('end--------------平铺路由信息， 数量: ' + this.routeList.length);
            console.info('start--------------加载alive-name信息');
            const routeNameList: string[] = [];
            this.routeList.forEach(f => {
                if (f.name && f.meta && f.meta.isKeepAlive) {
                    routeNameList.push(f.name.toString());
                }
            })
            useKeepALiveName(pinia).setCacheKeepAlive(routeNameList);
            console.info('end--------------加载alive-name信息');
            this.hasLoad = true;

        },
        getChildren: function (routerModel: RouteRecordRaw): RouteRecordRaw[] {
            const children: RouteRecordRaw[] | undefined = routerModel.children;
            if (undefined === children || useObjectUtil.isNull(children)) {
                return [];
            }
            let result: RouteRecordRaw[] = [];
            children.forEach(child => {
                result.push(child);
                const vchild = this.getChildren(child);
                vchild.forEach(v => result.push(v));
            });
            return result;
        },
    }
})