export interface StaticThemeConfigModel {
    globalTitle: string | undefined,
    globalViceTitle: string | undefined,
    isFooter: boolean | false,
}

export interface StaticThemeConfigsModel {
    config: StaticThemeConfigModel
}

export interface ConfigInfoSearchParamModel {
    configName: string | undefined,
    configCode: string | undefined,
}

export interface ConfigInfoListModel {
    id: string | undefined,
    configName: string | undefined,
    configCode: string | undefined,
    configValue: string | undefined,
    validFlag: number | undefined,
    createTime: Date | undefined,

}

export interface ConfigInfoDetailModel {
    id: string | undefined,
    configName: string | undefined,
    configCode: string | undefined,
    configValue: string | undefined,
    validFlag: number | undefined,
    remark: string | undefined,

}

export interface ConfigInfoAddParamModel {
    configName: string | undefined,
    configCode: string | undefined,
    configValue: string | undefined,
    validFlag: number | undefined,
    remark: string | undefined,
}

export interface ConfigInfoEditParamModel {
    id: string | undefined,
    configName: string | undefined,
    configValue: string | undefined,
    validFlag: number | undefined,
    remark: string | undefined,
}

export interface ConfigInfoDeleteParamModel {
    idList: string[],
}

export interface FileInfoSearchParamModel {
    fileName: string | undefined,
    platformId: string | undefined,
}

export interface FileInfoListModel {
    id: string | undefined,
    fileName: string | undefined,
    fileSize: string | undefined,
    fileExt: string | undefined,
    serverPath: string | undefined,
    platformId: string | undefined,
    validFlag: number | undefined,
    pageviewTimes: number | undefined,
    createTime: Date | undefined,

}

export interface FileInfoDetailModel {
    id: string | undefined,
    fileName: string | undefined,
    fileSize: string | undefined,
    fileExt: string | undefined,
    contentType: string | undefined,
    serverPath: string | undefined,
    platformId: string | undefined,
    validFlag: number | undefined,
    pageviewTimes: number | undefined,

}

export interface FileInfoAddParamModel {
    platformId: string | undefined,
}

export interface FileInfoDeleteParamModel {
    idList: string[],
}

export interface LogInfoSearchParamModel {
    fileName: string | undefined,
    operateNode: string | undefined,
}

export interface LogInfoListModel {
    id: string | undefined,
    fileName: string | undefined,
    operateType: number | undefined,
    operateNode: string | undefined,
    operateIp: string | undefined,
    createTime: Date | undefined,

}

export interface RegisterInfoSearchParamModel {
    nodeName: string | undefined,
    ip: string | undefined,
}

export interface RegisterInfoListModel {
    id: string | undefined,
    nodeName: string | undefined,
    ip: string | undefined,
    executeStatus: number | undefined,
    createTime: Date | undefined,

}

export interface RegisterInfoDeleteParamModel {
    idList: string[],
}

export interface PlatformInfoSearchParamModel {
    platformId: string | undefined,
    platformName: string | undefined,
}

export interface PlatformInfoListModel {
    id: string | undefined,
    platformId: string | undefined,
    platformName: string | undefined,
    validFlag: number | undefined,
    createTime: Date | undefined,

}

export interface PlatformInfoDetailModel {
    id: string | undefined,
    platformId: string | undefined,
    platformName: string | undefined,
    validFlag: number | undefined,
    secret: string | undefined,

}

export interface PlatformInfoAddParamModel {
    platformId: string | undefined,
    platformName: string | undefined,
    validFlag: number | undefined,
}

export interface PlatformInfoEditParamModel {
    id: string | undefined,
    platformId: string | undefined,
    platformName: string | undefined,
    validFlag: number | undefined,
}

export interface PlatformInfoDeleteParamModel {
    idList: string[],
}