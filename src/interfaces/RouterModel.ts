import { RouteRecordRaw } from 'vue-router';

export interface RouterMetaModel {
    title: string | undefined,
    isLink: string | undefined,
    isHide: boolean | false,
    isKeepAlive: boolean | false,
    isAffix: boolean | false,
    isIframe: boolean | false,
    roles: string[] | [],
    icon: string | '',
}

export interface RouterInfoModel {
    routeList: RouteRecordRaw[] | [],
    simpleDynamicRouteList: RouteRecordRaw[] | [],
    dynamicRouteNameSet: Set<string>,
    hasLoad: boolean | false,
    columnsNavHover: boolean,
    columnsMenuHover: boolean,
}

export interface TagViewInfoModel {
    tagViewList: RouteRecordRaw[] | [],
    isTagsViewCurrenFull: boolean | false,
    cacheChange: boolean | false,
}