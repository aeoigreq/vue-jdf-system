export interface LoginParamModel {
    username: string,
    password: string,
    loginType: string,
}

export interface EmployeeStore {
    token: string | null,
    loading: boolean,
    loadStartTime: number | 0,
    employee: EmployeeInfoDetailModel
}

export interface EmployeeInfoDetailModel {
    id: string | undefined,
    userId: string | undefined,
    employeeName: string | undefined,
    employeeTel: string | undefined,
    headUrl: string | undefined,
    employeeStatus: number | undefined,
    email: string | undefined,
    sex: number | undefined,
    orgList: OrgRelationModel[] | [],
    roleList: RoleRelationModel[] | [],
    funcIdList: string[] | [],
    createTime: Date,
    updateTime: Date,
}

export interface EmployeeUpdatePasswordModel {
    employeeId: string | undefined,
    password: string | undefined,
    newPassword: string | undefined,
    confirmPassword: string | undefined,
}

export interface OrgRelationModel {
    orgId: string | undefined,
    orgName: string | undefined,
    defaultOrgId: number | undefined,
    orgLevelCode: string | undefined,
}

export interface RoleRelationModel {
    roleId: string | undefined,
    roleName: string | undefined,
    defaultRoleId: number | undefined,
}

