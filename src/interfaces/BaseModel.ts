export interface PageInfo {
    page: number,
    limit: number,
    total: number,
}
export interface BaseRequest<T> {
    param: T,
    pageInfo: PageInfo | any
}
export interface BaseResponse<T> {
    code: string,
    msg: string,
    data: T,
    pageInfo: PageInfo
}