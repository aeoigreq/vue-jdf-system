import { RouteRecordRaw } from 'vue-router';

export const dynamicRoutes: RouteRecordRaw[] = [
    {
        path: '/',
        name: '/',
        component: () => import('/@/layout/index.vue'),
        redirect: '/home',
        meta: {
            title: '',
            isKeepAlive: true,
            isLink: '',
            isHide: false,
            isAffix: false,
            isIframe: false,
            roles: [],
            icon: '',
        },
        children: [
            {
                path: '/home',
                name: 'home',
                redirect: '',
                component: () => import('/@/views/home/index.vue'),
                meta: {
                    title: '首页',
                    isLink: '',
                    isHide: false,
                    isKeepAlive: true,
                    isAffix: true,
                    isIframe: false,
                    funcId: 'jdfHome',
                    icon: 'iconfont icon-shouye',
                },
                children: []
            },
            {
                path: '/register',
                name: 'register',
                component: () => import('/@/layout/routerView/parent.vue'),
                redirect: '/register',
                meta: {
                    title: '注册信息',
                    isLink: '',
                    isHide: false,
                    isKeepAlive: true,
                    isAffix: false,
                    isIframe: false,
                    funcId: 'jdfRegister',
                    icon: 'iconfont icon-xitongshezhi',
                },
                children: [
                    {
                        path: '/register',
                        name: 'register',
                        redirect: '',
                        component: () => import('/@/views/register-info/index.vue'),
                        meta: {
                            title: '注册信息',
                            isLink: '',
                            isHide: false,
                            isKeepAlive: true,
                            isAffix: false,
                            isIframe: false,
                            funcId: 'jdfRegister',
                            icon: 'ele-SetUp',
                        },
                        children: []
                    },
                    {
                        path: '/config',
                        name: 'config',
                        redirect: '',
                        component: () => import('/@/views/config-info/index.vue'),
                        meta: {
                            title: '配置管理',
                            isLink: '',
                            isHide: false,
                            isKeepAlive: true,
                            isAffix: false,
                            isIframe: false,
                            funcId: 'jdfConfig',
                            icon: 'ele-Setting',
                        },
                        children: []
                    },
                    {
                        path: '/file',
                        name: 'file',
                        redirect: '',
                        component: () => import('/@/views/file-info/index.vue'),
                        meta: {
                            title: '文件信息',
                            isLink: '',
                            isHide: false,
                            isKeepAlive: true,
                            isAffix: false,
                            isIframe: false,
                            funcId: 'jdfFile',
                            icon: 'ele-Document',
                        },
                        children: []
                    },
                    {
                        path: '/log',
                        name: 'log',
                        redirect: '',
                        component: () => import('/@/views/log-info/index.vue'),
                        meta: {
                            title: '日志信息',
                            isLink: '',
                            isHide: false,
                            isKeepAlive: true,
                            isAffix: false,
                            isIframe: false,
                            funcId: 'jdfLog',
                            icon: 'ele-Memo',
                        },
                        children: []
                    },
                    {
                        path: '/platform',
                        name: 'platform',
                        redirect: '',
                        component: () => import('/@/views/platform-info/index.vue'),
                        meta: {
                            title: '平台信息',
                            isLink: '',
                            isHide: false,
                            isKeepAlive: true,
                            isAffix: false,
                            isIframe: false,
                            funcId: 'jdfPlatform',
                            icon: 'ele-Connection',
                        },
                        children: []
                    },
                ],
            },
        ],
    },
];