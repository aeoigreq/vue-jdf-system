import request from '/@/utils/request';
import { useUrlUtil } from '/@/utils/urlUtil';
import { BaseResponse } from '/@/interfaces/BaseModel';
import { FileInfoApi } from '/@/enums/jdfsystem-api-enum';
import { BaseRequest } from '/@/interfaces/BaseModel';
import {
    FileInfoSearchParamModel,
    FileInfoListModel,
    FileInfoDetailModel,
    FileInfoAddParamModel,
    FileInfoDeleteParamModel
} from '/@/interfaces/JdfsystemModel';

function useFileInfo() {
    return {
        list: function (payload: BaseRequest<FileInfoSearchParamModel>) {
            return request.post<BaseResponse<FileInfoListModel[]>>(FileInfoApi.LIST, payload);
        },
        detail: function (payload: Object) {
            const useUrl = useUrlUtil();
            return request.get<BaseResponse<FileInfoDetailModel>>(useUrl.joinParam(FileInfoApi.DETAIL, payload));
        },
        add: function (payload: FileInfoAddParamModel) {
            return request.post<BaseResponse<boolean>>(FileInfoApi.ADD, payload);
        },
        del: function (payload: FileInfoDeleteParamModel) {
            return request.post<BaseResponse<boolean>>(FileInfoApi.DEL, payload);
        },
        view: function (fileId: string): string {
            return (import.meta.env.VITE_API_URL as any) + FileInfoApi.VIEW + fileId;
        },
        uploadUrl: function (): string {
            return (import.meta.env.VITE_API_URL as any) + FileInfoApi.UPLOAD;
        }
    };
}

export default useFileInfo;