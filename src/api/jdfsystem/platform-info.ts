import request from '/@/utils/request';
import { useUrlUtil } from '/@/utils/urlUtil';
import { BaseResponse } from '/@/interfaces/BaseModel';
import { PlatformInfoApi } from '/@/enums/jdfsystem-api-enum';
import { BaseRequest } from '/@/interfaces/BaseModel';
import {
    PlatformInfoSearchParamModel,
    PlatformInfoListModel,
    PlatformInfoDetailModel,
    PlatformInfoAddParamModel,
    PlatformInfoEditParamModel,
    PlatformInfoDeleteParamModel
} from '/@/interfaces/JdfsystemModel';

function usePlatformInfo() {
    return {
        list: function (payload: BaseRequest<PlatformInfoSearchParamModel>) {
            return request.post<BaseResponse<PlatformInfoListModel[]>>(PlatformInfoApi.LIST, payload);
        },
        detail: function (payload: Object) {
            const useUrl = useUrlUtil();
            return request.get<BaseResponse<PlatformInfoDetailModel>>(useUrl.joinParam(PlatformInfoApi.DETAIL, payload));
        },
        add: function (payload: BaseRequest<PlatformInfoAddParamModel>) {
            return request.post<BaseResponse<boolean>>(PlatformInfoApi.ADD, payload);
        },
        edit: function (payload: BaseRequest<PlatformInfoEditParamModel>) {
            return request.post<BaseResponse<boolean>>(PlatformInfoApi.EDIT, payload);
        },
        del: function (payload: BaseRequest<PlatformInfoDeleteParamModel>) {
            return request.post<BaseResponse<boolean>>(PlatformInfoApi.DEL, payload);
        },
    };
}

export default usePlatformInfo;