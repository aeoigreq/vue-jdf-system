import request from '/@/utils/request';
import { BaseResponse } from '/@/interfaces/BaseModel';
import { RegisterInfoApi } from '/@/enums/jdfsystem-api-enum';
import { BaseRequest } from '/@/interfaces/BaseModel';
import {
    RegisterInfoSearchParamModel,
    RegisterInfoListModel,
    RegisterInfoDeleteParamModel
} from '/@/interfaces/JdfsystemModel';

function useRegisterInfo() {
    return {
        list: function (payload: BaseRequest<RegisterInfoSearchParamModel>) {
            return request.post<BaseResponse<RegisterInfoListModel[]>>(RegisterInfoApi.LIST, payload);
        },
        del: function (payload: RegisterInfoDeleteParamModel) {
            return request.post<BaseResponse<boolean>>(RegisterInfoApi.DEL, payload);
        },
    };
}

export default useRegisterInfo;