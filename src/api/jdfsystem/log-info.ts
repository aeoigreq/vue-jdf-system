import request from '/@/utils/request';
import { BaseResponse } from '/@/interfaces/BaseModel';
import { LogInfoApi } from '/@/enums/jdfsystem-api-enum';
import { BaseRequest } from '/@/interfaces/BaseModel';
import {
    LogInfoSearchParamModel,
    LogInfoListModel,
} from '/@/interfaces/JdfsystemModel';

function useLogInfo() {
    return {
        list: function (payload: BaseRequest<LogInfoSearchParamModel>) {
            return request.post<BaseResponse<LogInfoListModel[]>>(LogInfoApi.LIST, payload);
        },
    };
}

export default useLogInfo;