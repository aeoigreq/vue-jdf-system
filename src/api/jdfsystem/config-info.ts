import request from '/@/utils/request';
import { useUrlUtil } from '/@/utils/urlUtil';
import { BaseResponse } from '/@/interfaces/BaseModel';
import { ConfigInfoApi } from '/@/enums/jdfsystem-api-enum';
import { BaseRequest } from '/@/interfaces/BaseModel';
import {
    ConfigInfoSearchParamModel,
    ConfigInfoListModel,
    ConfigInfoDetailModel,
    ConfigInfoAddParamModel,
    ConfigInfoEditParamModel,
    ConfigInfoDeleteParamModel
} from '/@/interfaces/JdfsystemModel';

function useConfigInfo() {
    return {
        list: function (payload: BaseRequest<ConfigInfoSearchParamModel>) {
            return request.post<BaseResponse<ConfigInfoListModel[]>>(ConfigInfoApi.LIST, payload);
        },
        detail: function (payload: Object) {
            const useUrl = useUrlUtil();
            return request.get<BaseResponse<ConfigInfoDetailModel>>(useUrl.joinParam(ConfigInfoApi.DETAIL, payload));
        },
        add: function (payload: BaseRequest<ConfigInfoAddParamModel>) {
            return request.post<BaseResponse<boolean>>(ConfigInfoApi.ADD, payload);
        },
        edit: function (payload: BaseRequest<ConfigInfoEditParamModel>) {
            return request.post<BaseResponse<boolean>>(ConfigInfoApi.EDIT, payload);
        },
        del: function (payload: BaseRequest<ConfigInfoDeleteParamModel>) {
            return request.post<BaseResponse<boolean>>(ConfigInfoApi.DEL, payload);
        },
    };
}

export default useConfigInfo;