import request from '/@/utils/request';
import { BaseResponse } from '/@/interfaces/BaseModel';
import { EmployeeInfoApi } from '/@/enums/systemservice-api-enum';
import {
    EmployeeInfoDetailModel,
    EmployeeUpdatePasswordModel,
} from '/@/interfaces/SystemserviceModel';

function useEmployeeInfo() {
    return {
        currentEmployee: function () {
            return request.post<BaseResponse<EmployeeInfoDetailModel>>(EmployeeInfoApi.CURRENT_EMPLOYEE, {});
        },
        editPassword: function(payload: EmployeeUpdatePasswordModel) {
            return request.post<BaseResponse<boolean>>(EmployeeInfoApi.UPDATE_PASSWORD, payload);
        }
    };
}

export default useEmployeeInfo;