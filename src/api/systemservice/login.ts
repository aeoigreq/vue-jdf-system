import request from '/@/utils/request';
import { BaseResponse } from '/@/interfaces/BaseModel';
import { LoginApi } from '../../enums/systemservice-api-enum';
import {
    LoginParamModel
} from '/@/interfaces/SystemserviceModel';

export const useLogin = {
    login: function (params: LoginParamModel) {
        return request.post<BaseResponse<string>>(LoginApi.LOGIN, params);
    },
    logout: function () {
        return request.get<BaseResponse<boolean>>(LoginApi.LOGOUT);
    }
}