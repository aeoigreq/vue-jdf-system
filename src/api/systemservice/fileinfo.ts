import { FileApi } from '/@/enums/systemservice-api-enum';
export function useFileInfoApi() {
    return {
        view: function (fileId: string): string {
            return (import.meta.env.VITE_API_URL as any) + FileApi.VIEW + fileId;
        },
        uploadUrl: function (): string {
            return (import.meta.env.VITE_API_URL as any) + FileApi.UPLOAD;
        }
    }
}