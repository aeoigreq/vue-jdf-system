export function useUrlUtil() {
    return {
        joinParam: function (url: string, obj: any): string {
            const params: string[] = [];
            Object.keys(obj).forEach((key) => {
                let value = obj[key]
                // 如果值为undefined我们将其置空
                if (typeof value !== 'undefined') {
                    // 对于需要编码的文本（比如说中文）我们要进行编码
                    params.push([key, encodeURIComponent(value)].join('='))
                }
            })
            return url + '?' + params.join('&')
        }
    }
};