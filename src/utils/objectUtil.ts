
const useObjectUtil = {
  isNull: function (data: any) {
    return '' === data || undefined === data || null === data;
  },
  isNotNull: function (data: any) {
    return !useObjectUtil.isNull(data);
  },

  /**
   * 拷贝目标对象有的属性
   * @param source 源对象
   * @param target 目标对象
   * @returns 
   */
  copyProperties: function (source: { [k: string]: any } = {}, target: { [k: string]: any } = {}) {
    if (useObjectUtil.isNull(source)) {
      return;
    }
    if (useObjectUtil.isNull(target)) {
      target = {};
    }
    const keysTo = Object.keys(target);
    if (keysTo.length <= 0) {
      Object.assign(target, source);
      return;
    }

    for (let key of keysTo) {
      if (source[key] !== undefined) {
        target[key] = source[key];
      }
    }
  },

  /**
   * 完全拷贝一个对象的属性，原来的所有属性都会先清除
   * @param target 目标对象
   * @param source 源对象
   * @returns 
   */
  assignAll: function (target: { [k: string]: any } = {}, source: { [k: string]: any } = {}) {
    if (useObjectUtil.isNull(source)) {
      return ;
    }
    const targetKeysTo = Object.keys(target);
    if(targetKeysTo.length >0) {
      for(let key of targetKeysTo) {
        delete target[key];
      }
    }
    const keysTo = Object.keys(source);
    if (keysTo.length <= 0) {
      return;
    }

    for (let key of keysTo) {
      if (source[key] !== undefined) {
        target[key] = source[key];
      }
    }
  },
}

export default useObjectUtil