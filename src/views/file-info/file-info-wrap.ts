import type { FormInstance } from 'element-plus';
import { ElMessage } from 'element-plus';
import useFileInfo from '/@/api/jdfsystem/file-info';
import MESSAGE from '/@/enums/message-enum';
import { BaseRequest } from '/@/interfaces/BaseModel';
import {
    FileInfoSearchParamModel,
    FileInfoAddParamModel,
} from '/@/interfaces/JdfsystemModel';

const handle = useFileInfo();

async function handleSearch(form: BaseRequest<FileInfoSearchParamModel>) {
    const response = await handle.list(form);
    return response.data;
}

async function handleDetail(id: string) {
    const response = await handle.detail({ id: id });
    return response.data;
}

async function handleAdd(form: FileInfoAddParamModel, callback: () => void) {
    const response = await handle.add(form);
    const { code, msg } = response.data;
    if (MESSAGE.OK === code) {
        ElMessage.success(msg);
        callback();
    }
    return response.data;
}

async function handleDel(idList: string[]) {
    const response = await handle.del({ idList: idList });
    const { code, msg } = response.data;
    if (MESSAGE.OK === code) {
        ElMessage.success(msg);
    }
    return response.data;
}

async function handleValidateForm(formRef: FormInstance | undefined) {
    let flag = false;
    if (!formRef) {
        return flag;
    }
    await formRef.validate((valid, fields) => {
        console.debug(fields);
        flag = valid;
    });
    return flag;
}

export {
    handleSearch,
    handleDetail,
    handleAdd,
    handleDel,
    handleValidateForm
}
