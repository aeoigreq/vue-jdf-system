import { ElMessage } from 'element-plus';
import pinia from '/@/stores/index';
import { useLogin } from '/@/api/systemservice/login';
import useEmployeeInfo from '/@/api/systemservice/employee-info';
import { useEmployeeStore } from '/@/stores/employee-info';
import MESSAGE from '/@/enums/message-enum';
import { formatAxis } from '/@/utils/formatTime';
import { NextLoading } from '/@/utils/loading';
import type { FormInstance } from 'element-plus';


async function handleLogin(loading: boolean, params: any) {
    loading = true;
    const res = await useLogin.login(params);
    const { code, data } = res.data;
    if (code === MESSAGE.OK) {
        console.info('Token=' + data);
        await afterLogin(data);
    }
    // 关闭loading
    loading = false;
}

async function afterLogin(token: string) {
    // 保存到session
    useEmployeeStore(pinia).setToken(token);
    await currentEmployee();

    // 登录成功提示
    let currentTimeInfo = formatAxis(new Date());
    const signInText = '欢迎回来！';
    ElMessage.success(`${currentTimeInfo}，${signInText}`);
    NextLoading.start();
}

async function currentEmployee() {
    const useEmployee = useEmployeeInfo();
    const res = await useEmployee.currentEmployee();
    const { code, data } = res.data;
    if (MESSAGE.OK === code) {
        useEmployeeStore(pinia).setEmployeeInfo(data);
    }
}

async function validateForm(formRel: FormInstance | undefined) {
    let flag = false;
    if (!formRel) {
        return flag;
    }
    await formRel.validate((valid, fields) => {
        console.info(fields);
        flag = valid;
    });
    return flag;
}

export {
    validateForm,
    handleLogin
}