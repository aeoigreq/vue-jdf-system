import { ElMessage } from 'element-plus';
import useRegisterInfo from '/@/api/jdfsystem/register-info';
import MESSAGE from '/@/enums/message-enum';
import { BaseRequest } from '/@/interfaces/BaseModel';
import {
    RegisterInfoSearchParamModel,
} from '/@/interfaces/JdfsystemModel';

const handle = useRegisterInfo();

async function handleSearch(form: BaseRequest<RegisterInfoSearchParamModel>) {
    const response = await handle.list(form);
    return response.data;
}

async function handleDel(idList: string[]) {
    const response = await handle.del({ idList: idList });
    const {code, msg} = response.data;
    if(MESSAGE.OK === code) {
        ElMessage.success(msg);
    }
    return response.data;
}

export {
    handleSearch,
    handleDel,
}
