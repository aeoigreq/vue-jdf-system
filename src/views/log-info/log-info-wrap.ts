import { BaseRequest } from '/@/interfaces/BaseModel';
import useLogInfo from '/@/api/jdfsystem/log-info';
import {
    LogInfoSearchParamModel,
} from '/@/interfaces/JdfsystemModel';

const handle = useLogInfo();

async function handleSearch(form: BaseRequest<LogInfoSearchParamModel>) {
    const response = await handle.list(form);
    return response.data;
}

export {
    handleSearch,
}
