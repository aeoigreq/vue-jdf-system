import type { FormInstance } from 'element-plus';
import { ElMessage } from 'element-plus';
import useConfigInfo from '/@/api/jdfsystem/config-info';
import MESSAGE from '/@/enums/message-enum';
import { BaseRequest } from '/@/interfaces/BaseModel';
import {
    ConfigInfoSearchParamModel,
    ConfigInfoAddParamModel,
    ConfigInfoEditParamModel,
} from '/@/interfaces/JdfsystemModel';

const handle = useConfigInfo();

async function handleSearch(form: BaseRequest<ConfigInfoSearchParamModel>) {
    const response = await handle.list(form);
    return response.data;
}

async function handleDetail(id: string) {
    const response = await handle.detail({ id: id });
    return response.data;
}

async function handleAdd(form: ConfigInfoAddParamModel, callback: () => void) {
    const response = await handle.add({ param: form, pageInfo: null });
    const { code, msg } = response.data;
    if (MESSAGE.OK === code) {
        ElMessage.success(msg);
        callback();
    }
    return response.data;
}

async function handleEdit(form: ConfigInfoEditParamModel, callback: () => void) {
    const response = await handle.edit({ param: form, pageInfo: null });
    const { code, msg } = response.data;
    if (MESSAGE.OK === code) {
        ElMessage.success(msg);
        callback();
    }
    return response.data;
}

async function handleDel(idList: string[]) {
    const response = await handle.del({ param: { idList: idList }, pageInfo: null });
    const { code, msg } = response.data;
    if (MESSAGE.OK === code) {
        ElMessage.success(msg);
    }
    return response.data;
}

async function handleValidateForm(formRef: FormInstance | undefined) {
    let flag = false;
    if (!formRef) {
        return flag;
    }
    await formRef.validate((valid, fields) => {
        console.debug(fields);
        flag = valid;
    });
    return flag;
}

export {
    handleSearch,
    handleDetail,
    handleAdd,
    handleEdit,
    handleDel,
    handleValidateForm
}
