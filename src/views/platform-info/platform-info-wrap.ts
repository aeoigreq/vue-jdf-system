import type { FormInstance } from 'element-plus';
import { ElMessage } from 'element-plus';
import usePlatformInfo from '/@/api/jdfsystem/platform-info';
import MESSAGE from '/@/enums/message-enum';
import { BaseRequest } from '/@/interfaces/BaseModel';
import {
    PlatformInfoSearchParamModel,
    PlatformInfoAddParamModel,
    PlatformInfoEditParamModel,
} from '/@/interfaces/JdfsystemModel';

const handle = usePlatformInfo();

async function handleSearch(form: BaseRequest<PlatformInfoSearchParamModel>) {
    const response = await handle.list(form);
    return response.data;
}

async function handleDetail(id: string) {
    const response = await handle.detail({ id: id });
    return response.data;
}

async function handleAdd(form: PlatformInfoAddParamModel, callback: () => void) {
    const response = await handle.add({ param: form, pageInfo: null });
    const { code, msg } = response.data;
    if (MESSAGE.OK === code) {
        ElMessage.success(msg);
        callback();
    }
    return response.data;
}

async function handleEdit(form: PlatformInfoEditParamModel, callback: () => void) {
    const response = await handle.edit({ param: form, pageInfo: null });
    const { code, msg } = response.data;
    if (MESSAGE.OK === code) {
        ElMessage.success(msg);
        callback();
    }
    return response.data;
}

async function handleDel(idList: string[]) {
    const response = await handle.del({ param: { idList: idList }, pageInfo: null });
    const { code, msg } = response.data;
    if (MESSAGE.OK === code) {
        ElMessage.success(msg);
    }
    return response.data;
}

async function handleValidateForm(formRef: FormInstance | undefined) {
    let flag = false;
    if (!formRef) {
        return flag;
    }
    await formRef.validate((valid, fields) => {
        console.debug(fields);
        flag = valid;
    });
    return flag;
}

export {
    handleSearch,
    handleDetail,
    handleAdd,
    handleEdit,
    handleDel,
    handleValidateForm
}
