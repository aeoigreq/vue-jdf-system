enum CONSTANTS {
    Authorization= 'Authorization',
    LOGIN_PAGE_NAME= 'login',
    HOME_PAGE_NAME= '/',
    SESSION_IS_Tag_View_Curren_Full= 'S_IS_Tag_View_Curren_Full',
    SESSION_TAG_VIEW_LIST= 'S_TAG_VIEW_LIST',
    SESSION_DICT_INFO = "S_DICT_INFO",
    SESSION_EMPLOYEE_INFO = "S_EMPLOYEE_INFO",
}

export default CONSTANTS;