import { RouteRecordName } from 'vue-router';
import pinia from '/@/stores/index';
import { useEmployeeStore } from '/@/stores/employee-info';
import CONSTANTS from '/@/enums/global-constant-enum';

export enum RouterAction {
    NoLogin = 1,
    NoLoginHaveParam = 2,
    Go = 3,
}

export const useRouterAction = {
    forRouterAction: function (name: RouteRecordName  | null | undefined, targetPath: string): RouterAction {
        const employeeStore = useEmployeeStore(pinia);
        const hasLogin = employeeStore.getHasLogin();
        if (!hasLogin && name === CONSTANTS.LOGIN_PAGE_NAME) {
            return RouterAction.NoLogin;
        }
        if (!hasLogin) {
            return RouterAction.NoLoginHaveParam;
        }
        return RouterAction.Go;
    }
}