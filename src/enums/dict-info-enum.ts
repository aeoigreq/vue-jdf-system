export enum DictType {
    SEX = 'sex',
    HTTP_METHOD = 'httpMethod',
    YES_OR_NO = 'yesOrNo',
    LOG_OPERATE_TYPE = 'logOperateType',
    EXECUTE_STATUS = 'executeStatus',
}

export enum RegisterExecuteStatus {
    DOWN_LINE = 0,
    EXCEPTION = 1,
    NORMAL = 2,
}

export const DictInfo = {
    registerExecuteStatus: [
        {
            key: 0,
            label: '下线',
        },
        {
            key: 1,
            label: '异常',
        },
        {
            key: 2,
            label: '正常',
        },
    ],
    yesOrNo: [
        {
            key: 1,
            label: '是',
        },
        {
            key: 0,
            label: '否'
        }
    ],
    logOperateType: [
        {
            key: 1,
            label: '新增',
        },
        {
            key: 2,
            label: '删除',
        },
    ]
}