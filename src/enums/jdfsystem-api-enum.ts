export enum ConfigInfoApi {
    LIST = '/api/jdfsystem/jdf/config/list',
    DETAIL = '/api/jdfsystem/jdf/config/detail',
    ADD = '/api/jdfsystem/jdf/config/add',
    EDIT = '/api/jdfsystem/jdf/config/edit',
    DEL = '/api/jdfsystem/jdf/config/delete',
}

export enum FileInfoApi {
    LIST = '/api/jdfsystem/jdf/file/list',
    DETAIL = '/api/jdfsystem/jdf/file/detail',
    ADD = '/api/jdfsystem/jdf/file/upload',
    DEL = '/api/jdfsystem/jdf/file/delete',
    VIEW = 'api/jdfsystem/jdf/file/view/',
    UPLOAD = 'api/jdfsystem/jdf/file/upload',
}

export enum LogInfoApi {
    LIST = '/api/jdfsystem/jdf/log/list',
    DETAIL = '/api/jdfsystem/jdf/log/detail',
    ADD = '/api/jdfsystem/jdf/log/add',
    EDIT = '/api/jdfsystem/jdf/log/edit',
    DEL = '/api/jdfsystem/jdf/log/delete',
}

export enum RegisterInfoApi {
    LIST = '/api/jdfsystem/jdf/register/list',
    DETAIL = '/api/jdfsystem/jdf/register/detail',
    ADD = '/api/jdfsystem/jdf/register/add',
    EDIT = '/api/jdfsystem/jdf/register/edit',
    DEL = '/api/jdfsystem/jdf/register/delete',
}

export enum PlatformInfoApi {
    LIST = '/api/jdfsystem/jdf/platformInfo/list',
    DETAIL = '/api/jdfsystem/jdf/platformInfo/detail',
    ADD = '/api/jdfsystem/jdf/platformInfo/add',
    EDIT = '/api/jdfsystem/jdf/platformInfo/edit',
    DEL = '/api/jdfsystem/jdf/platformInfo/delete',
}