export enum LoginApi {
    LOGIN = '/api/auth/login',
    LOGOUT = '/api/auth/logout',
}

export enum FileApi {
    UPLOAD = 'api/storage/file/upload',
    VIEW = 'api/storage/file/view?key=',
}

export enum EmployeeInfoApi {
    CURRENT_EMPLOYEE = '/api/systemservice/employeeInfo/getByOperator',
    UPDATE_PASSWORD = '/api/systemservice/employeeInfo/updatePassword',
}
