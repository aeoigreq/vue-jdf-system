enum MESSAGE {
    OK = '200',
    BAD_REQUEST = '400',
    NO_LOGIN = '401'
}

export default MESSAGE;